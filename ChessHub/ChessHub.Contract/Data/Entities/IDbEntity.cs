﻿using System;

namespace ChessHub.Contract.Data.Entities
{
    public interface IDbEntity
    {
        Guid Id { get; set; }
    }
}
