﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessHub.Contract.Data.Entities;

namespace ChessHub.Contract.Data.Access
{
    public interface IRepository<T> where T: class, IDbEntity
    {
        IQueryable<T> All();

        T GetById(Guid id);

        void Add(T entity);

        void Update(T entity);

        void Delete(T entity);

        void Delete(Guid id);

    }
}
