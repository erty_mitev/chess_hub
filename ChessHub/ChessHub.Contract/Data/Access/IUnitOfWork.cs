﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessHub.Contract.Data.Entities;

namespace ChessHub.Contract.Data.Access
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> Repository<TEntity>() where TEntity : class, IDbEntity;

        int SaveChanges();
    }
}
