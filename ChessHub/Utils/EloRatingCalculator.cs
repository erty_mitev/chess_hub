﻿using System;
using System.Collections.Generic;
using ChessHub.Data.Entities;

namespace Utils
{
    public class EloRatingCalculator
    {
        private static byte ReturnKFactor(int rating)
        {
            if (rating < 2100)
            {
                return 32;
            }

            if (rating < 2400)
            {
                return 24;
            }

            return 16;
        }

        private static double[] ReturnActualElo(GameResult gameResult)
        {
            double[] result = new double[2];
            switch (gameResult)
            {
                case GameResult.WhiteVictory:
                    result[0] = ActualRating[GameResult.WhiteVictory];
                    result[1] = ActualRating[GameResult.BlackVictory];
                    break;
                case GameResult.BlackVictory:
                    result[0] = ActualRating[GameResult.BlackVictory];
                    result[1] = ActualRating[GameResult.WhiteVictory];
                    break;
                default:
                    result[0] = ActualRating[GameResult.GameDrawn];
                    result[1] = ActualRating[GameResult.GameDrawn];
                    break;
            }
            return result;
        }

        private static double[] ExpectedRatingPts(int WhitePLayerRating, int BlackPlayerRating)
        {
            // Calculating Elo with true strength(current rating) of the players
            double difference = WhitePLayerRating - BlackPlayerRating;
            double[] eloRating = new double[2];
            eloRating[0] = 1 / (1 + Math.Pow(10, (difference / 400)));
            eloRating[1] = 1 / (1 + Math.Pow(10, ((-1 * difference) / 400)));

            return eloRating;
        }

        //Dictionary for win/losses/ties points
        private static readonly Dictionary<GameResult, double> ActualRating = new Dictionary<GameResult, double>
        {
            {GameResult.WhiteVictory, 1 },
            {GameResult.BlackVictory, 0 },
            {GameResult.GameDrawn, 0.5 }
        };

        //Calculation of the Final Elo rating
        public static int[] RatingChanges(Player whitePlayer, Player blackPlayer, GameResult gameResult)
        {
            byte whiteKFactor = ReturnKFactor(whitePlayer.EloRating);
            byte blackKFactor = ReturnKFactor(blackPlayer.EloRating);

            int[] finalRatings = new int[2];
            finalRatings[0] = whitePlayer.EloRating + (int)(whiteKFactor * (ReturnActualElo(gameResult)[0] - ExpectedRatingPts(whitePlayer.EloRating, blackPlayer.EloRating)[0]));
            finalRatings[1] = blackPlayer.EloRating + (int)(blackKFactor * (ReturnActualElo(gameResult)[1] - ExpectedRatingPts(whitePlayer.EloRating, blackPlayer.EloRating)[1]));
            return finalRatings;
        }
    }
}
