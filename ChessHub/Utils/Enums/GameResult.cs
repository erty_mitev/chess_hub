﻿namespace Utils.Enums
{
    public enum GameResult
    {
        WhiteVictory = 0,
        BlackVictory = 1,
        GameDrawn = 2,
    }
}
