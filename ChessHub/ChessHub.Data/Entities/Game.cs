﻿using System;
using System.Collections.Generic;
using ChessHub.Contract.Data.Entities;

namespace ChessHub.Data.Entities
{
    public class Game: Deletable, IDbEntity
    {
        public Guid Id { get; set; }

        public GameResult Result { get; set; }

        public Player White { get; set; }

        public Player Black { get; set; }

        public int TimeControl { get; set; }

        public virtual ICollection<Move> Moves { get; set; }
        
    }
}
