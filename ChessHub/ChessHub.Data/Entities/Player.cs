﻿using System;
using System.Collections.Generic;
using ChessHub.Contract.Data.Entities;

namespace ChessHub.Data.Entities
{
    public class Player: Deletable, IDbEntity
    {
        public Guid Id { get; set; }

        public int EloRating { get; set; }

        public string DisplayName { get; set; }

        public virtual ICollection<Game> Games { get; set; }

    }
}
