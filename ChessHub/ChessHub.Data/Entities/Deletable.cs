﻿using System;
using ChessHub.Contract.Data.Entities;

namespace ChessHub.Data.Entities
{
    public abstract class Deletable: IDeletable
    {
        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}
