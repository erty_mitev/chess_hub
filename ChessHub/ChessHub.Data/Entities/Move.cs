﻿using System;
using ChessHub.Contract.Data.Entities;

namespace ChessHub.Data.Entities
{
    public class Move: Deletable, IDbEntity
    {
        public Guid Id { get; set; }

        public int MoveIndex { get; set; }

        public bool IsCastling { get; set; }

        public string PieceNotation { get; set; }

        public byte StartPosition { get; set; }

        public byte EndPosition { get; set; }

        public int GameId { get; set; }

        public virtual Game Game { get; set; }

    }
}
