﻿using ChessHub.Contract.Data.Access;
using ChessHub.Contract.Data.Entities;
using ChessHub.Data.Contexts;
using ChessHub.Data.Repositories;
using System;
using System.Collections.Generic;

namespace ChessHub.Data.UnitOfWork
{
    public class UnitOfWork: IUnitOfWork
    {
        private SiteDbContext _context;

        private Dictionary<Type, object> _repositories;

        public UnitOfWork(SiteDbContext context)
        {
            this._context = context;
            this._repositories = new Dictionary<Type, object>();
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : class, IDbEntity
        {
            _repositories.Add(
                typeof(TEntity),
                new Repository<TEntity>(_context)
            );

           return (IRepository<TEntity>)_repositories[typeof(TEntity)];
        }

        public int SaveChanges()
        {
            return this._context.SaveChanges();
        }

    }
}
