﻿using ChessHub.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace ChessHub.Data.Contexts
{
    public class SiteDbContext: IdentityDbContext<ApplicationUser>
    {
        public SiteDbContext(DbContextOptions<SiteDbContext> options)
         : base(options)
        { }

        public DbSet<Player> Players { get; set; }

        public DbSet<Game> Games { get; set; }

        public DbSet<Move> Moves { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Player>().HasKey(p => p.Id);
            modelBuilder.Entity<Player>().HasMany(p => p.Games);

            modelBuilder.Entity<Game>().HasKey(g => g.Id);
            modelBuilder.Entity<Game>().HasOne(g => g.White);
            modelBuilder.Entity<Game>().HasOne(g => g.Black);
            modelBuilder.Entity<Game>().HasMany(g => g.Moves);

            modelBuilder.Entity<Move>().HasKey(m => m.Id);
            modelBuilder.Entity<Move>().HasOne(m => m.Game);

        }
    }

}
