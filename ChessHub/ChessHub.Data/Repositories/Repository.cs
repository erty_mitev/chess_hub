﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChessHub.Contract.Data.Access;
using ChessHub.Contract.Data.Entities;
using ChessHub.Data.Contexts;
using Microsoft.EntityFrameworkCore;

namespace ChessHub.Data.Repositories
{
    public class Repository<T> :IRepository<T> where T: class, IDbEntity
    {

        private readonly SiteDbContext _context;
        private readonly DbSet<T> _dbSet;

        public Repository(SiteDbContext context)
        {
            this._context = context;
            this._dbSet = this._context.Set<T>();
        }

        public IQueryable<T> All()
        {
            return this._dbSet.AsQueryable();
        }

        public T GetById(Guid id)
        {
            return this._dbSet.Find(id);
        }

        public void Add(T entity)
        {
            this._dbSet.Add(entity);
        }

        public void Update(T entity)
        {
            var entry = this._context.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                this._dbSet.Attach(entity);
            }

            entry.State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            var entry = this._context.Entry(entity);
            if (entry.State != EntityState.Deleted)
            {
                entry.State = EntityState.Deleted;
            }
            else
            {
                this._dbSet.Attach(entity);
                this._dbSet.Remove(entity);
            }
        }

        public void Delete(Guid id)
        {
            var entity = this.GetById(id);

            if (entity != null)
            {
                this.Delete(entity);
            }
        }

    }
}
