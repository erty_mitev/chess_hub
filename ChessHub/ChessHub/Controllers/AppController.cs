﻿using System;
using System.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace ChessHub.Web.Controllers
{
    public class AppController: Controller
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public AppController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        [Route("app")]
        [HttpGet]
        public ActionResult App()
        {
            string webRootPath = _hostingEnvironment.WebRootPath;
            string contentRootPath = _hostingEnvironment.ContentRootPath;

            return PhysicalFile(webRootPath + "\\App\\home.html", "text/html");
        }

        
    }
}
