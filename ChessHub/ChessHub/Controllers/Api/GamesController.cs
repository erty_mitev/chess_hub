﻿using ChessHub.Contract.Data.Access;
using ChessHub.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ChessHub.Web.Controllers.Api
{
    [Produces("application/json")]
    public class GamesController : Controller
    {
        private readonly IUnitOfWork _uow;

        public GamesController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [HttpGet]
        [Route("api/Games")]
        public IActionResult GetAllGames()
        {
            IRepository<Game> repo = _uow.Repository<Game>();

            return new ObjectResult(repo.All().ToList().ToList());
        }
    }
}