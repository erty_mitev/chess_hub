angular.module('userServices',[])
.factory('User',function($http){
    var userFactory = {};
    //User.create(regData)
    userFactory.create = function(regData){
        return $http.post('/api/users',regData);
    }

    return userFactory;
})
.factory('Auth',function($http){
    var authFactory = {};
    //Auth.login(loginData)
    authFactory.login = function(loginData){
        return $http.post('/api/authenticate',loginData);
    }
    return authFactory;
})