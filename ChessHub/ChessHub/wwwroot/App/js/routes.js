angular.module('appRoutes', ['ngRoute'])
    .config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '../App/views/main.html',

            })
            .when('/login',{
                templateUrl: '../App/views/login.html',
                controller: 'loginController',
                controllerAs: 'login'
            })
            .when('/register',{
                templateUrl: '../App/views/register.html',
                controller: 'registerController',
                controllerAs: 'register'
            })
            .otherwise({
                redirectTo: '/'
            });

    });